//Funcion para mostrar y ocultar secciones
function mostrar(seccion) {
  	var x = document.getElementById(seccion);
  	if (x.style.display === "none") {
    	x.style.display = "block";
  	}
    if (seccion !== "cero") {
        var x0 = document.getElementById("cero");
        x0.style.display = "none";
    }
  	if (seccion !== "uno") {
    	var x1 = document.getElementById("uno");
    	x1.style.display = "none";
    }
    if (seccion !== "dos") {
    	var x2 = document.getElementById("dos");
    	x2.style.display = "none";
    }
    if (seccion !== "tres") {
    	var x3 = document.getElementById("tres");
    	x3.style.display = "none";
    }
    if (seccion !== "cuatro") {
    	var x4 = document.getElementById("cuatro");
    	x4.style.display = "none";
    }
    if (seccion !== "cinco") {
    	var x5 = document.getElementById("cinco");
    	x5.style.display = "none";
    }
}